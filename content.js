window.hrefs = {};

$(document).ready(function () {

    var emails = {};
    var phones = {};

    //find all website URLs on each Google search result page
    var googlelinks = document.querySelectorAll(".r");
    for (let i = 0; i < googlelinks.length; i++) {
        var a = document.querySelectorAll(".r")[i].getElementsByTagName("a")[0].getAttribute('href');
        hrefs[a] = true;
    }

    //find all website URLs on each Ecosia search result page
    var ecoLinks = document.querySelectorAll(".result-snippet-link");
    for (let i = 0; i < ecoLinks.length; i++) {
        var a = document.querySelectorAll(".result-snippet-link")[i].getAttribute('href');
        hrefs[a] = true;
    }

    // //find all website URLs on each DuckDuckGo search result page
    // var more = document.getElementsByClassName("result--more__btn btn btn--full")[0].click();
    // var duckLinks = document.querySelectorAll(".result__extras__url");
    // for (let i = 0; i < duckLinks.length; i++) {
    //     var a = document.querySelectorAll(".result__extras__url")[i].getElementsByTagName("a")[1].getAttribute('href');
    //     hrefs[a] = true;
    // }

    //find all website URLs on each Bing search result page
    if (Object.keys(hrefs).length === 0) {
        var bingLinks = document.getElementsByTagName("cite");
        for (let i = 0; i < bingLinks.length; i++) {
            var a = document.getElementsByTagName("cite")[i].innerText;
            hrefs[a] = true;
        }
    }

    //send all these URLs to the popup script
    if (Object.keys(hrefs).length > 0) {
        var jsonLinks = JSON.stringify(hrefs);  //convert to JSON object
        chrome.runtime.sendMessage({
            data: jsonLinks
        });
    }

    function scrape() {
        console.log('scraping');
        var phoneArr = [
            "\\+\\d\\d?-? ?\\d{3}-\\d{3}-\\d{4}",//
            "\\+\\d\\d?-? ?\\d{10}",
            "\\+\\d\\d?-? ?\\d{5} \\d{5}",
            "\\s\\d{3}-\\d{3}-\\d{4}\\s",
            "\\s\\d{10}\\s",
            "\\s\\d{5} \\d{5}\\s"
        ];
        var re = new RegExp(phoneArr.join("|"), "g");
        var selector = 'body';
        var email_return_arr;
        var phone_return_arr;
        var email_matches = $(selector).html().match(/[\w._%+-]+@[\w-]+(\.[\w]{2,10})+/g);
        var phone = $(selector).text().match(re);
        if (email_matches !== null)
            email_return_arr = [...new Set(email_matches.map(d => d.toLowerCase()))];
        if (phone !== null)
            phone_return_arr = [...new Set(phone)];

        if (email_return_arr !== undefined) {
            for (let e = 0; e < email_return_arr.length; e++) {
                emails[email_return_arr[e]] = true;
            }
        }
        var emailsObj = JSON.stringify(emails);

        if (phone_return_arr !== undefined) {
            for (let p = 0; p < phone_return_arr.length; p++) {
                phones[phone_return_arr[p]] = true;
            }
        }
        var phonesObj = JSON.stringify(phones);

        //send the scraped emails and phone numbers to the popup script
        chrome.runtime.sendMessage({
            emaildata: emailsObj,
            phonedata: phonesObj
        });
    }

    //listen for the message from popup script and then call the scrape() function
    chrome.runtime.onMessage.addListener(
        function (request) {
            if (request.call == "search_result_url_loaded") {
                $(document).ready(function () {
                    scrape();
                });
            }
        });

});