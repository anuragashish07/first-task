var locations = ['bangalore', 'delhi', 'noida', 'pune', 'coimbatore', 'new york', 'patna'];
var skills = {
    'yoga': 'yogis',
    'engineering': 'engineers'
};
var keywords = ['@gmail.com', '+91'];

var randomLocation;
var randomSkill;
var randomKey;
var query;
var emails = {};
var contacts = {};
var topSearchURLList = {};

search_pages_loop = 5;
google_search_loop_counter = 0;
search_engines_supported = ['google', 'ecosia', 'bing'];
setTimeoutInterval = 5000;
setTimeoutCount = 0;
search_engine_URLs = [];

//generate a search query using random keywords and convert it into an URL
function generatequery() {
    randomLocation = Math.floor(Math.random() * locations.length);
    randomSkill = Math.floor(Math.random() * Object.keys(skills).length);
    randomKey = Math.floor(Math.random() * keywords.length);
    var URL = (locations[randomLocation] + " " + Object.keys(skills)[randomSkill] + " " + keywords[randomKey]).toString();
    return URL;
}

function generateURLS(query) {
    for (let main_search_loop = 0; main_search_loop < search_engines_supported.length; main_search_loop++) {
        if (search_engines_supported[main_search_loop] == 'google') {
            console.log('entering GOOGLE ' + (setTimeoutCount * (search_pages_loop + 1) * setTimeoutInterval));
            generateGurls(query);
        }
        if (search_engines_supported[main_search_loop] == 'ecosia') {
            console.log('entering ECOSIA ' + (setTimeoutCount*(search_pages_loop+1)*setTimeoutInterval));
            generateEurls(query);
        }
        if (search_engines_supported[main_search_loop] == 'bing') {
            console.log('entering BING ' + (setTimeoutCount * (search_pages_loop + 1) * setTimeoutInterval));
            generateBurls(query);
        }
        // if (search_engines_supported[main_search_loop] == 'duckduckgo') {
        //     generateDurls(query);
        // }
        setTimeoutCount++; 
    }
}


function generateGurls(query) {
    var googleURL = ('https://www.google.com/search?q=' + query).toString();
    //loop through the other consecutive search result pages
    for (var google_search_loop_counter = 0; google_search_loop_counter < search_pages_loop; google_search_loop_counter++) {
        console.log('GOOGLE GENERATE URL' + google_search_loop_counter);
        var nextURL = (googleURL + "&start=" + (google_search_loop_counter * 10)).toString();
        search_engine_URLs.push(nextURL);
    }
}

function generateEurls(query) {
    var ecosiaURL = ('https://www.ecosia.org/search?&q=' + query).toString();
    //loop through the other consecutive search result pages
    for (var search_loop_counter = 0; search_loop_counter < search_pages_loop; search_loop_counter++) {
        console.log('ECOSIA GENERATE URL' + search_loop_counter);
        var URL = (ecosiaURL + "&p=" + (search_loop_counter)).toString();
        search_engine_URLs.push(URL);
    }
}

function generateBurls(query) {
    var bingURL = ('https://www.bing.com/search?q=' + query).toString();
    //loop through the other consecutive search result pages
    for (var search_loop_counter = 0; search_loop_counter < search_pages_loop; search_loop_counter++) {
        console.log('BING GENERATE URL' + search_loop_counter);
        var URL = (bingURL + "&first=" + ((search_loop_counter * 10) + 1)).toString();
        search_engine_URLs.push(URL);
    }
    if (search_loop_counter === search_pages_loop) {
        console.log(search_engine_URLs);        
    }
}

// function generateDurls(query) {
//     var duckURL = ('https://duckduckgo.com/?q=' + query).toString();
//     search_engine_URLs.push(duckURL);
//     console.log(search_engine_URLs);
// }

function call_every_search_url() {
    let call_every_search_url_loop_count = -1;
    setInterval(function () {
        call_every_search_url_loop_count++;
        if (call_every_search_url_loop_count < search_engine_URLs.length) {
            var nextURL = (search_engine_URLs[call_every_search_url_loop_count]).toString();
            chrome.tabs.update({ url: nextURL });
        }
        if (call_every_search_url_loop_count === search_engine_URLs.length) {
            console.log(topSearchURLList); 
            console.log(Object.keys(topSearchURLList).length);
            scrapeCall();
        }
    }, Math.floor(Math.random() * setTimeoutInterval) + 2000);
       
}

function search_main() {
    console.log('hello');
    var query = generatequery();
    generateURLS(query);
    // for (var i = 0; i < search_engine_URLs.length; i++) {
    //     setTimeout(call_every_search_url, i * setTimeoutInterval);
    // }
    setTimeout(call_every_search_url, setTimeoutInterval);
    //setTimeout(scrapeCall, (search_engine_URLs.length * setTimeoutInterval * search_pages_loop + 20000));
}


(function () {
    document.addEventListener('DOMContentLoaded', function () {
        document.querySelector('#start').addEventListener(
            'click', search_main);
    });
})();

//listen for URLs being returned for each search result page from the content script
chrome.runtime.onMessage.addListener(function (msg) {
    if (msg.data !== undefined) {
        var links = JSON.parse(msg.data);
        console.log("links received");
        for (let j = 0; j < Object.keys(links).length; j++) {
            topSearchURLList[Object.keys(links)[j]] = true; //add all URLs to the finalList[]
        }
    }
});

//loop through each URL stored in topSearchURLList[] at random intervals and call scrape function for each URL
function scrapeCall() {
    var i = -1;
    setInterval(function () {
        i++;
        if (i < Object.keys(topSearchURLList).length) {
            var nextURL = Object.keys(topSearchURLList)[i];
            chrome.tabs.update({ url: nextURL });
            chrome.tabs.query({ active: true, currentWindow: true }, function (tabs) {
                chrome.tabs.sendMessage(tabs[0].id, { call: "search_result_url_loaded" }); //send message to content script to start scraping
            });
            console.log("Scraping call sent");

        }
    }, Math.floor(Math.random() * 3000) + 3000);
}

//listen for scraped data being returned from content script
chrome.runtime.onMessage.addListener(function (message) {
    if (message.emaildata !== undefined || message.phonedata !== undefined) {
        var emailsrec = JSON.parse(message.emaildata);
        for (let e = 0; e < Object.keys(emailsrec).length; e++) {
            emails[Object.keys(emailsrec)[e]] = true;
        }

        var phonesrec = JSON.parse(message.phonedata);
        for (let p = 0; p < Object.keys(phonesrec).length; p++) {
            contacts[Object.keys(phonesrec)[p]] = true;
        }

        console.log("final email list", emails);
        console.log("number of emails received= ", Object.keys(emailsrec).length);
        console.log("final contact list", contacts);
        console.log("number of contacts received= ", Object.keys(phonesrec).length);
    }
});